package resources;

import dao.UserDAO;
import data.User;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * User resource exposed at "/user" path
 */
@Path("user")
public class UserResource {

    /**
     * Method handling HTTP GET requests
     * @return List of users as JSON response
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getUsers(){
        UserDAO userDAO = new UserDAO();
        return userDAO.getUsers();
    }

    /**
     * Method handling HTTP POST requests
     * @param user userInformation as String
     * @return the new User if not registered, or the User that matches the username as JSON object
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User newUser(User user){
        UserDAO userDAO = new UserDAO();
        return userDAO.addUser(user);
    }

    /**
     * Method handling HTTP PUT requests
     * @param userId userId as int
     * @param user user as a User object, which is to be changed
     * @return Boolean indicating whether the userId was changed
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{userId}")
    public boolean editUser(@PathParam("userId") int userId, User user){
        UserDAO userDAO = new UserDAO();
        return userDAO.editUser(userId, user.getUsername(), user.getPassword());
    }
}
