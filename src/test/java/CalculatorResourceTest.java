import org.junit.jupiter.api.Test;
import resources.CalculatorResource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 100 ";
        assertEquals(200, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99+100";
        assertEquals(499, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2-3";
        assertEquals(15, calculatorResource.subtraction(expression));

        expression = "20-2-3-20";
        assertEquals(-5, calculatorResource.subtraction(expression));
    }
    @Test
    public void testDivision() {//TODO: Bad for loop, implement further functionality on other operators.
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "100/10";
        assertEquals(10, calculatorResource.division(equation));

        equation = "100/10/10";
        assertEquals(1, calculatorResource.division(equation));

        String testEquation = "100/0";
        assertThrows(ArithmeticException.class, () -> new CalculatorResource().division(testEquation));

        String testEquationTwo =  "apeshit/100";
        assertEquals(0, calculatorResource.division(testEquationTwo));

    }

    @Test
    public void testMultiplication() {
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "100*5";
        assertEquals(500, calculatorResource.multiplication(equation));

        equation = "50*3";
        assertEquals(150, calculatorResource.multiplication(equation));

        equation = "50*3*100*3";
        assertEquals(45000, calculatorResource.multiplication(equation));
    }
}
